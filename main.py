from ui import main_screen, except_screen
from tendo import singleton


try:
    me = singleton.SingleInstance() # will sys.exit(-1) if other instance is running
    check = True
except:
    check = False



def start_script():
    if check:
        main_screen.Window()
    else:
        except_screen.Window('Уже запущено!')




if __name__ == '__main__':
    start_script()
