from domain.usecase.noun_list_creator import build_short_words_list


class Filter:
    def __init__(self, grey_chars: str, white_chars: tuple, yellow_chars: tuple, words: list):
        """
        grey_chars: список букв, которых точно нет в слове.
        white_chars: список букв, которые точное есть в слове, но на др. местах.
        yellow_chars: список букв, которые точно есть в слове и расположены в нужных местах.
        words: полный справочник слов из файла \"russian.txt\". 
        return: self.result - список отфильтрованных по всем фильтрам слов.
        """
        self.short_words_list = build_short_words_list(words)

        self.grey_list = Filter.grey_filter(self, grey_chars)
        self.white_list = Filter.white_filter(self, white_chars)
        self.result = Filter.yellow_filter(self, yellow_chars)


    def grey_filter(self, grey_chars: str) -> list:
        grey_list = []
        
        for word in self.short_words_list:
            save = True
            for char in grey_chars:
                if char in word:
                    save = False
            if save:
                grey_list.append(word)

        return grey_list


    def white_filter(self, white_chars: tuple) -> list:
        qty_chars = len([item for item in white_chars if item != "" and item != " "])
        white_list = []
        
        for word in self.grey_list:
            number_of_matches = 0
            for indx, val in enumerate(white_chars):
                if (val != "" 
                    and val != " "
                    and val in word 
                    and white_chars[indx] != word[indx]):
                    number_of_matches += 1
            if qty_chars == number_of_matches:
                white_list.append(word)

        return white_list


    def yellow_filter(self, yellow_chars: tuple) -> list:
        qty_chars = len([item for item in yellow_chars if item != "" and item != " "])

        yellow_list = []

        for word in self.white_list:
            number_of_matches = 0
            for indx, val in enumerate(yellow_chars):
                if (val != "" 
                    and val != " "
                    and yellow_chars[indx] == word[indx]):
                    number_of_matches += 1
            if qty_chars == number_of_matches:
                yellow_list.append(word)

        return yellow_list
