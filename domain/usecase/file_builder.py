from os import path
from domain.usecase.noun_list_creator import create_noun_words_list


class WordsFile:
    file_name = "russian.txt"

    def __init__(self):
        if not path.exists(self.file_name):
            WordsFile.save(self)

    def save(self):
        wl = create_noun_words_list()
        with open(self.file_name, "w", encoding="cp1251") as file:
            data_to_write = "\n".join(wl)
            file.write(data_to_write)

    def read(self) -> list:
        words_list = []
        with open(self.file_name, "r", encoding="cp1251") as file:
            words_list = file.read().split("\n")
        return words_list
