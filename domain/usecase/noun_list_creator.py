import requests
from bs4 import BeautifulSoup # type: ignore


def get_noun_words_from_html_page(html_page) -> list:
    soup = BeautifulSoup(html_page, "html.parser")
    position_obj = soup.findAll("div", class_="position_title")
    
    words_list = []
    for data in position_obj:
        words_list.append(data.text.split()[0])
    return words_list
        
def build_short_words_list(words_list: list) -> list:
    short_list = []
    for word in words_list:
        if len(word) == 5:
            short_list.append(word)
    return short_list


def create_noun_words_list() -> list:
    list_letter = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯ"

    words_list = []
    for letter in list_letter:
        res = requests.get(f"https://kupidonia.ru/spisok/spisok-suschestvitelnyh-russkogo-jazyka/bukva/{letter}")
        if res.status_code == 200:
            html_page = res.text
            full_wl = get_noun_words_from_html_page(html_page)
            for item in full_wl:
                words_list.append(item)
    return words_list

