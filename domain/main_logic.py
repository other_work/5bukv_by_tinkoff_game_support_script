from domain.usecase.filter_listing import Filter
from domain.usecase.file_builder import WordsFile


grey_chars = "сукнота"
white_chars = ("п", "", "и", "ц", "")
yellow_chars = ("", "", "", "", "")

wl = WordsFile().read()
sl = Filter(grey_chars, white_chars, yellow_chars, wl).result

print(sl)
print("Желтый спиисок содержит слов: ", len(sl))
