# Игра 5 букв от Тинькофф

Требуется разгадать загадонное существительное из 5 букв.  
Даётся 6 попыток.

## Описание скрипта

Скрипт - призван помочь фильтровать ненужные слова (отсеивая лишние).

Принцип:

* **Серый** - список букв, которых точно нет в слове.  
-> *формат ввода "трпаед" (без кавычек)*
* **Белый** - список букв, которые точное есть в слове, но на др. местах.  
-> *форма ввода " .с.у. .н" (без кавычек/пробел, где нет букв под данный фильтр)*
* **Желтый** - список букв, которые точно есть в слове и расположены в нужных местах.  
-> *форма ввода "с.у. .н.о" (без кавычек/пробел, где нет букв под данный фильтр)*

Для корректной работы, необхоим наиболее полный список слов, расположенный
в каталоге с исполняемым файлом и имеющий наимннование `russian.txt`.  
Файл должен быть формата - каждое отдельное слово, в отдельной строке.
Кодировка `Windows 1251`.

Если файл со списком слов не был обнаружен - скрипт сформирует его автоматически.  
*Для формирования используется сайт <https://kupidonia.ru>, а точнее
раздел с существительными словами  
<https://kupidonia.ru/spisok/spisok-suschestvitelnyh-russkogo-jazyka/bukva/%7Bletter%7D>  
**  
Как вариант, воспользоваться готовым файлом `russian.txt` с ресурса  
<https://github.com/danakt/russian-words>  
В нём большее кол-во слов, но при этом - не только существительные.  
**  
Важно! Скрипт, не определяет, является ли слово существительным.*

## Используемые сторонние библиотеки

* **requests** - используется получения и копирования страниц сайта со словами
* **bs4** - используется для парсинга слов со страниц
* **tendo** - используется для блокировки повторного запуска скрипта
* **pyinstaller** - для формирования сборок исполняемых файлов
