from tkinter import *


def focus_in_entry(label: Label, key: int) -> bool:
    if key == 0:
        info_text = StringVar(value='Введите строковый набор букв\n формата "йцуфы" (без кавычек).')
    elif key == 1 or key == 2:
        info_text = StringVar(value='Введите строчные буквы\n формата "й. .у.ф.ы" (без кавычек).')
    else:
        info_text = StringVar(value='')

    # область с краткой подсказкой
    label.config(bg='black', relief= SOLID, textvariable=info_text)
    return True
