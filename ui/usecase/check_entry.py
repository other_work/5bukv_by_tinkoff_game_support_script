from tkinter import *
from tkinter import Event


def check_release(event: Event, entries: list[Entry], indx: int):
    """
Функция для обработки события перед нажатием кнопки в поле ввода
    Args:
        event (Event): содержит параметры события
        entries (list[Entry]): список объёктов полей ввода
        indx (int): индекс списка для поля ввода
    """

    entry: Entry = entries[indx]
    fld: str = entry.get()
    if indx != 0:
        if (event.keysym != 'BackSpace'
                and event.keysym != 'Delete' and event.keysym != 'Return'):
            for key, char in enumerate(fld):
                if key == 1 and char != '.':
                    entry.insert(key, '.')
                elif key == 3 and char != '.':
                    entry.insert(key, '.')
                elif key == 5 and char != '.':
                    entry.insert(key, '.')
                elif key == 7 and char != '.':
                    entry.insert(key, '.')
                elif key > 8:
                    entry.delete("9", END)
        elif ((event.keysym == 'BackSpace' or event.keysym == 'Delete')
              and len(fld) > 0 and fld[-1] == '.'):
            entry.delete(len(fld) - 1, END)


def check_press(event, entries: list[Entry], indx: int):
    """
Функция для обработки события после нажатия кнопки в поле ввода
    Args:
        event (Event): содержит параметры события
        entries (list[Entry]): список объёктов полей ввода
        indx (int): индекс списка для поля ввода
    """

    entry: Entry = entries[indx]
    fld: str = entry.get()
    if (event.keysym != 'BackSpace' and event.keysym != 'Delete'
            and event.keysym != 'Tab' and event.keysym != 'Return'):
        for key, char in enumerate(fld):
            if key > 7:
                entry.delete("8", END)
