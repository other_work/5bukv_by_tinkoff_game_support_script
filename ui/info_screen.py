from tkinter import *
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText


class Window(Toplevel):
    faq_text = """Игра 5 букв.
Требуется разгадать загадонное существительное из 5 букв.
Даётся 6 попыток.
                      
Скрипт помогает фильтровать слова, отсеивая лишние.
Принцип:
* Серый - список букв, которых точно нет в слове.
-> формат ввода "трпаед" (без кавычек)
* Белый - список букв, которые точное есть в слове, но на др. местах.
-> форма ввода " .с.у. .н" (без кавычек/пробел, где нет букв под данный фильтр)
* Желтый - список букв, которые точно есть в слове и расположены в нужных местах.
-> форма ввода "с.у. .н.о" (без кавычек/пробел, где нет букв под данный фильтр)
                      
Для корректной работы, необхоим наиболее полный справочник слов, расположенный 
в каталоге с исполняемым файлом и имеющий наимннование \"russian.txt\". 
Файл должен быть формата - каждое отдельное слово, в отдельной строке.
Кодировка Windows 1251.

Важно! Скрипт, не определяет, является ли слово существительным.
                      
"""
    width_ = 765
    height_ = 380
    def __init__(self) -> None:
        super().__init__()
        # создаём форму
        self.title(f"О утилите")
        self.geometry(f"{self.width_}x{self.height_}+900+300")
        self.update_idletasks()
        self.resizable(False, False) # выключает растягивание по ширине / высоте
        self.attributes("-alpha", 0.95)
        self.configure(bg="orange")

        # размещение элементов
        Window.elements(self)
        
        # обработка событий
        Window.event_processing(self)

    def elements(self):
        # фрагмент/контейнер
        frame = ttk.Frame(self, relief=SOLID)
        frame.place(relx=0.5, rely=0.5, 
                        anchor='center',
                        width=self.width_-10, height=self.height_-20)

        # поле вывода многострочной информации
        editor = ScrolledText(frame, relief=SOLID)
        editor.place(relx=.5, rely=.5, 
                anchor="c", 
                width=self.width_-15, height=self.height_-25)

        editor.delete("1.0", END)
        editor.insert('1.0', self.faq_text)

    def event_processing(self):
        self.protocol("WM_DELETE_WINDOW", lambda: Window.dismiss(self)) # перехватываем нажатие на крестик
        self.bind("<Escape>", lambda e: Window.dismiss(self)) # закрытие окна по кнопке Escape
        self.grab_set() # захватываем пользовательский ввод

    def dismiss(self):
        self.grab_release() 
        self.destroy()
