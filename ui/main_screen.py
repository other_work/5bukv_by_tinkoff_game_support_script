from tkinter import *
from tkinter import ttk
from ui.usecase.check_entry import check_release, check_press
from ui.usecase.view_faq import focus_in_entry
from ui import info_screen, result_screen
from domain.usecase.filter_listing import Filter
from domain.usecase.file_builder import WordsFile


class Window(Tk):
    def __init__(self) -> None:
        super().__init__(className='5bukovok: game support script')

        # создаём основное окно
        self.title("5Буковок")
        self.geometry("300x200+800+350")
        self.update_idletasks()
        self.resizable(False, False) # выключает растягивание по ширине / высоте
        self.attributes("-alpha", 0.99) # прозрачность окна

        # размещение элементов
        Window.elements(self)
        
        # устновка цветов
        Window.collor_setings(self)

        # обработка событий
        Window.event_processing(self)

        # петлица
        self.mainloop()

    def elements(self):
        # фрагмент/контейнер для полей ввода
        input_frame = ttk.Frame(relief=SOLID)
        input_frame.place(relx=0.5, rely=0.37, 
                        anchor='center',
                        width=280, height=65)

        ## сетка для полей ввода и их заголовков внутри фрагмента
        for c in range(3): input_frame.columnconfigure(index=c, weight=2)
        for r in range(2): input_frame.rowconfigure(index=r, weight=2)

        ## поля ввода и их заголовки внутри фрагмента
        self.entries = []
        self.labels = []
        for indx, name in enumerate(("Серый", "Белый", "Желтый")):
            lable_entry = Label(input_frame, text=name, relief=SOLID)
            lable_entry.grid(row=0, column=indx, columnspan=1, ipadx=6, ipady=6, padx=4, pady=4, sticky=NSEW)
            self.labels.append(lable_entry)
            entry_txt = Entry(input_frame)
            entry_txt.grid(row=1, column=indx, columnspan=1, ipadx=6, ipady=6, padx=4, pady=4)
            self.entries.append(entry_txt)

        # прочие элементы окна
        ## кнопка инфо - справа сверху
        self.btn_info = ttk.Button(text="i", width=3)
        self.btn_info.pack(anchor="se", padx=2, pady=2)

        ## кнопка получить - по середине, под полями ввода
        self.btn_get_result = ttk.Button(text="Получить?!")
        self.btn_get_result.place(relx=.5, rely=.65, 
                anchor="c", 
                width=110, height=30)
        
        ## поле отображения подсказок
        self.info_text = Label(bg="dodger blue", fg='white')
        self.info_text.place(relx=.5, rely=.95, anchor="s")

    def collor_setings(self):
        ## общий фон
        self.configure(bg="dodger blue") 

        ## общий стиль элементов
        ttk.Style().configure(".",  font="helvetica 9 bold", foreground="black", background="orange") 

        ## заколовки полей ввода
        self.labels[0].config(foreground= "white", background= "grey55")
        self.labels[1].config(foreground= "black", background= "white")
        self.labels[2].config(foreground= "black", background= "yellow")

    def event_processing(self):
        ## перед нажатием в поле ввода
        self.entries[0].bind('<KeyRelease>', 
                        lambda e: check_release(e, self.entries, 0))
        self.entries[1].bind('<KeyRelease>', 
                        lambda e: check_release(e, self.entries, 1))
        self.entries[2].bind('<KeyRelease>', 
                        lambda e: check_release(e, self.entries, 2))

        ## после нажатия в поле ввода
        self.entries[1].bind('<KeyPress>', 
                        lambda e: check_press(e, self.entries, 1))
        self.entries[2].bind('<KeyPress>', 
                        lambda e: check_press(e, self.entries, 2))

        ## при установке фокуса на поле ввода
        self.entries[0].config(validate="focusin", validatecommand= lambda: focus_in_entry(self.info_text, 0))
        self.entries[1].config(validate="focusin", validatecommand= lambda: focus_in_entry(self.info_text, 1))
        self.entries[2].config(validate="focusin", validatecommand= lambda: focus_in_entry(self.info_text, 2))

        ## при нажатии кнопок
        self.btn_get_result.config(command= lambda: result_screen.Window(Window.get_list_filtered_words(self)))
        self.btn_info.config(command= info_screen.Window)

        ## при нажатии на enter
        self.bind("<Return>", lambda e: result_screen.Window(Window.get_list_filtered_words(self)))

    def get_list_filtered_words(self):
        input_result = []
        for entry in self.entries:
            input_result.append(entry.get())
        grey_chars = input_result[0]
        white_chars = input_result[1].split('.')
        yellow_chars = input_result[2].split('.')

        words_list = WordsFile().read()
        return Filter(grey_chars, white_chars, yellow_chars, words_list).result
