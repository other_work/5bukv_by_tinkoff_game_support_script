from tkinter import *
from tkinter import ttk


class Window(Tk):
    def __init__(self, except_text: str):
        super().__init__()
        # форма
        self.geometry("250x100+800+350")
        self.title('5Буковок')
        self.update_idletasks()
        self.resizable(False, False) # выключает растягивание по ширине / высоте

        # устновка цветов
        ttk.Style().configure(".",  font="helvetica 9 bold", foreground="black", background="orange") 
        self.configure(bg="dodger blue")

        # кнопка
        button = ttk.Button(text='оК !', command=lambda: Window.button_close(self))
        button.pack(side=BOTTOM, pady=15)

        # надпись
        label = Label()
        label.place(anchor="center", relx=.5, rely=.35)
        label.configure(text=except_text, foreground= "white", background= "dodger blue", font="helvetica 11 bold")

        # петлица для запуска
        self.mainloop()

    def button_close(self):
        self.destroy()

