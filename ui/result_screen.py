from tkinter import *
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText


class Window(Toplevel):
    width_ = 765
    height_ = 380
    def __init__(self, words_list: list[str]) -> None:
        super().__init__()

        # создаём форму
        self.title(f"Результат: {len(words_list)}")
        self.geometry(f"{self.width_}x{self.height_}+900+300")
        self.update_idletasks()
        self.resizable(False, False) # выключает растягивание по ширине / высоте
        self.attributes("-alpha", 0.95)
        self.configure(bg="orange")
        
        # размещение элементов
        Window.elements(self, words_list)

        # обработка событий
        Window.event_processing(self)        

    def elements(self, words_list: list[str]):
        # фрагмент/контейнер
        frame = ttk.Frame(self, relief=SOLID)
        frame.place(relx=0.5, rely=0.5, 
                        anchor='center',
                        width=self.width_-10, height=self.height_-20)

        # поле вывода многострочной информации
        editor = ScrolledText(frame, relief=SOLID)
        editor.place(relx=.5, rely=.5, 
                anchor="c", 
                width=self.width_-15, height=self.height_-25)

        editor.delete("1.0", END)
        editor.insert('1.0', words_list)

    def event_processing(self):
        self.protocol("WM_DELETE_WINDOW", lambda: Window.dismiss(self)) # перехватываем нажатие на крестик
        self.bind("<Escape>", lambda e: Window.dismiss(self)) # закрытие окна по кнопке Escape
        self.grab_set() # захватываем пользовательский ввод

    def dismiss(self):
        self.grab_release() 
        self.destroy()
